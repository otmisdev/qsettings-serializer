#include <QCoreApplication>
#include <QDebug>

#include "settingsserializer.h"
#include "foo.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QString conf("custom.conf");
    // get the singleton instance
    SettingsSerializer *serializer = SettingsSerializer::getInstance(conf);
    // auto-register a specialist
    // add/write an object in custom.conf
    Foo f;
    f.setObjectName("SpecialHam");
    SettingsSerializer::Status status = serializer->registerObject(f);

    if ( status != SettingsSerializer::Ok ) {
        qFatal("Cannot register object in settings serializer");
    }

    f.setName("Special Ham");
    f.setCode(15);
    f.setPrice(22.99);
    f.setWeight(1.350);
    // delete the singleton instance and all specialists related
    SettingsSerializer::deleteInstance(conf);

    // get the singleton instance
    serializer = SettingsSerializer::getInstance(conf);
    // auto-register a specialist
    // recover the object from custom.conf
    Foo specialHam;
    specialHam.setObjectName("SpecialHam");
    serializer->registerObject(specialHam);

    qDebug() << "name" << specialHam.name() << "code" << specialHam.code()
             << "price" << specialHam.price() << "weight" << specialHam.weight();

    SettingsSerializer::deleteInstance(conf);
    return app.exec();
}
