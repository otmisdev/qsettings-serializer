QT += core

CONFIG += C++11

include(../../settingsserializer.pri)

INCLUDEPATH += ../../shared/include

HEADERS += ../../shared/include/foo.h

SOURCES += main.cpp \
           ../../shared/src/foo.cpp
