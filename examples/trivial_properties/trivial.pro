QT += core

CONFIG += C++11

include(../../settingsserializer.pri)

INCLUDEPATH += ../../shared/include

HEADERS += ../../shared/include/trivial.h

SOURCES += main.cpp \
           ../../shared/src/trivial.cpp
