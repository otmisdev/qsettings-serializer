#include <QCoreApplication>

#include "settingsserializer.h"
#include "trivial.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    QString conf("trivial.conf");
    Trivial obj;
    obj.setObjectName("Eiffel");
    SettingsSerializer *serializer = SettingsSerializer::getInstance(conf);
    SettingsSerializer::Status status = serializer->registerObject(obj);
    if ( status != SettingsSerializer::Ok ) {
        qFatal("Cannot register object in settings serializer");
    }
    obj.setPoi("Eiffel Tower");
    obj.setLatitude(48.858093);
    obj.setLongitude(2.294694);
    SettingsSerializer::deleteInstance( "trivial.conf" );
    //
    Trivial eiffel;
    eiffel.setObjectName("Eiffel");
    //
    serializer = SettingsSerializer::getInstance(conf);
    serializer->registerObject(eiffel);
    qDebug() << "point of interest" << eiffel.poi()
             << "latitude" << eiffel.latitude()
             << "longitude" << eiffel.longitude();
    //
    SettingsSerializer::deleteInstance( "trivial.conf" );
    //
    return app.exec();
}
