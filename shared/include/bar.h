#ifndef BAR_H
#define BAR_H

#include <QObject>
#include <QtQml>

class Bar : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(JobStatus status READ status WRITE setStatus NOTIFY statusChanged)
    Q_ENUMS(JobStatus)

public:
    enum class JobStatus {
        New = 0,
        Open,
        OnHold,
        Resolved,
        Duplicated,
        Invalid,
        WontFix,
        Closed
    };

    Bar();

    QString title() const
    {
        return m_title;
    }

    JobStatus status() const
    {
        return m_status;
    }

    static int declareQmlSingletonType() {
        return qmlRegisterSingletonType<Bar>("bar", 1, 0, "Bar",
                                             bar_singletontype_provider);
    }

    static QObject *bar_singletontype_provider(QQmlEngine *,
                                               QJSEngine *)
    {
        Bar *obj = new Bar;
        return obj;
    }

public slots:
    void setTitle(QString title)
    {
        if (m_title == title)
            return;

        m_title = title;
        emit titleChanged(title);
    }

    void setStatus(JobStatus status)
    {
        if (m_status == status)
            return;

        m_status = status;
        emit statusChanged(status);
    }

signals:
    void titleChanged(QString title);
    // This method has a wrong signature, the signature should be Bar::JobStatus
    void statusChanged(JobStatus status);

private:
    QString m_title;
    JobStatus m_status;
};

#endif // BAR_H
