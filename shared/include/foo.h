#ifndef FOO_H
#define FOO_H

#include <QObject>
#include <QString>

class Foo : public QObject {
    Q_OBJECT
    Q_PROPERTY(bool status READ status WRITE setStatus NOTIFY statusChanged STORED true)
    Q_PROPERTY(qint8 code READ code WRITE setCode NOTIFY codeChanged STORED true)
    Q_PROPERTY(quint8 ucode READ ucode WRITE setUcode NOTIFY ucodeChanged STORED true)
    Q_PROPERTY(qint16 code16 READ code16 WRITE setCode16 NOTIFY code16Changed STORED true)
    Q_PROPERTY(quint16 ucode16 READ ucode16 WRITE setUcode16 NOTIFY ucode16Changed STORED true)
    Q_PROPERTY(qint32 code32 READ code32 WRITE setCode32 NOTIFY code32Changed STORED true)
    Q_PROPERTY(quint32 ucode32 READ ucode32 WRITE setUcode32 NOTIFY ucode32Changed STORED true)
    Q_PROPERTY(qint64 code64 READ code64 WRITE setCode64 NOTIFY code64Changed STORED true)
    Q_PROPERTY(quint64 ucode64 READ ucode64 WRITE setUcode64 NOTIFY ucode64Changed STORED true)
    Q_PROPERTY(float price READ price WRITE setPrice NOTIFY priceChanged STORED true)
    Q_PROPERTY(double weight READ weight WRITE setWeight NOTIFY weightChanged STORED true)
    // the property x will not saved in settings file
    Q_PROPERTY(int x READ x WRITE setX NOTIFY xChanged STORED false)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged STORED true)
    Q_PROPERTY(State state READ state WRITE setState NOTIFY stateChanged STORED true)
    Q_ENUMS(State)

public:

    enum class State {
        Ok = 0,
        Simple,
        Complex,
        Unknown
    };

    explicit Foo(QObject *parent = 0);

    int x() const
    {
        return m_x;
    }

    QString name() const
    {
        return m_name;
    }

    bool status() const
    {
        return m_status;
    }

    qint8 code() const
    {
        return m_code;
    }

    quint8 ucode() const
    {
        return m_ucode;
    }

    qint16 code16() const
    {
        return m_code16;
    }

    quint16 ucode16() const
    {
        return m_ucode16;
    }

    qint32 code32() const
    {
        return m_code32;
    }

    quint32 ucode32() const
    {
        return m_ucode32;
    }

    qint64 code64() const
    {
        return m_code64;
    }

    quint64 ucode64() const
    {
        return m_ucode64;
    }

    float price() const
    {
        return m_price;
    }

    double weight() const
    {
        return m_weight;
    }

    State state() const
    {
        return m_state;
    }

signals:

    void xChanged(int x);

    void yChanged(int y);

    void nameChanged(QString name);

    void statusChanged(bool status);

    void codeChanged(qint8 code);

    void ucodeChanged(quint8 ucode);

    void code16Changed(qint16 code16);

    void ucode16Changed(quint16 ucode16);

    void code32Changed(qint32 code32);

    void ucode32Changed(quint32 ucode32);

    void code64Changed(qint64 code64);

    void ucode64Changed(quint64 ucode64);

    void priceChanged(float price);

    void weightChanged(double weight);

    void stateChanged(Foo::State state);

public slots:
    void setX(int x)
    {
        if (m_x == x)
            return;

        m_x = x;
        emit xChanged(x);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setStatus(bool status)
    {
        if (m_status == status)
            return;

        m_status = status;
        emit statusChanged(status);
    }

    void setCode(qint8 code)
    {
        if (m_code == code)
            return;

        m_code = code;
        emit codeChanged(code);
    }

    void setUcode(quint8 ucode)
    {
        if (m_ucode == ucode)
            return;

        m_ucode = ucode;
        emit ucodeChanged(ucode);
    }

    void setCode16(qint16 code16)
    {
        if (m_code16 == code16)
            return;

        m_code16 = code16;
        emit code16Changed(code16);
    }

    void setUcode16(quint16 ucode16)
    {
        if (m_ucode16 == ucode16)
            return;

        m_ucode16 = ucode16;
        emit ucode16Changed(ucode16);
    }

    void setCode32(qint32 code32)
    {
        if (m_code32 == code32)
            return;

        m_code32 = code32;
        emit code32Changed(code32);
    }

    void setUcode32(quint32 ucode32)
    {
        if (m_ucode32 == ucode32)
            return;

        m_ucode32 = ucode32;
        emit ucode32Changed(ucode32);
    }

    void setCode64(qint64 code64)
    {
        if (m_code64 == code64)
            return;

        m_code64 = code64;
        emit code64Changed(code64);
    }

    void setUcode64(quint64 ucode64)
    {
        if (m_ucode64 == ucode64)
            return;

        m_ucode64 = ucode64;
        emit ucode64Changed(ucode64);
    }

    void setPrice(float price)
    {
        if (m_price == price)
            return;

        m_price = price;
        emit priceChanged(price);
    }

    void setWeight(double weight)
    {
        if (m_weight == weight)
            return;

        m_weight = weight;
        emit weightChanged(weight);
    }

    void setState(State state)
    {
        if (m_state == state)
            return;

        m_state = state;
        emit stateChanged(state);
    }

private:
    bool m_status;
    qint8 m_code;
    quint8 m_ucode;
    qint16 m_code16;
    quint16 m_ucode16;
    qint32 m_code32;
    quint32 m_ucode32;
    qint64 m_code64;
    quint64 m_ucode64;
    float m_price;
    double m_weight;
    int m_x;
    QString m_name;
    State m_state;
};

#endif // FOO_H
