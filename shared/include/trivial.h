#ifndef TRIVIAL_H
#define TRIVIAL_H

#include <QDebug>
#include <QObject>


class Trivial : public QObject {
    Q_OBJECT
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(QString poi READ poi WRITE setPoi NOTIFY poiChanged)

public:
    Trivial();

    double latitude() const
    {
        return m_latitude;
    }

    double longitude() const
    {
        return m_logitude;
    }

    QString poi() const
    {
        return m_poi;
    }

public slots:
    void setLatitude(double latitude)
    {
        if (m_latitude == latitude)
            return;

        m_latitude = latitude;
        emit latitudeChanged(latitude);
    }

    void setLongitude(double logitude)
    {
        if (m_logitude == logitude)
            return;

        m_logitude = logitude;
        emit longitudeChanged(logitude);
    }

    void setPoi(QString poi)
    {
        if (m_poi == poi)
            return;

        m_poi = poi;
        emit poiChanged(poi);
    }

signals:
    void latitudeChanged(double latitude);
    void longitudeChanged(double longitude);
    void poiChanged(QString poi);

private:
    double m_latitude;
    double m_logitude;
    QString m_poi;
};

#endif // TRIVIAL_H
