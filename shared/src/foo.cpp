#include "foo.h"

Foo::Foo(QObject *parent)
    : QObject(parent)
    , m_status(false)
    , m_code(0)
    , m_ucode(0)
    , m_code16(0)
    , m_ucode16(0)
    , m_code32(0)
    , m_ucode32(0)
    , m_code64(0)
    , m_ucode64(0)
    , m_price(0.0)
    , m_weight(0.0)
    , m_x(0)
    , m_name(QString())
    , m_state(State::Unknown)
{

}

