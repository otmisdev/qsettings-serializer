#include "bar.h"
#include "settingsserializer.h"

Bar::Bar()
    : m_title(QString(""))
    , m_status(JobStatus::New)
{
    setObjectName("barObject");
    SettingsSerializer *serializer = SettingsSerializer::getInstance("test.conf");
    SettingsSerializer::Status status = serializer->registerObject(*this);
    qDebug() << "register status" << status;
    if ( status != SettingsSerializer::Ok ) {
        qCritical() << "failed to register object";
    }
}

