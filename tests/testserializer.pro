QT += core testlib qml quick gui

CONFIG += c++11

INCLUDEPATH += ../shared/include

HEADERS += ../shared/include/foo.h \
           ../shared/include/bar.h \
    ../shared/include/trivial.h

SOURCES = testserializer.cpp \
          ../shared/src/foo.cpp \
          ../shared/src/bar.cpp \
    ../shared/src/trivial.cpp

include(../settingsserializer.pri)

RESOURCES += \
    resources.qrc
