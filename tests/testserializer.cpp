#include <QtTest>
#include <QFile>
#include <QQuickView>

#include "settingsserializer.h"

#include "bar.h"
#include "foo.h"

class TestSerializer : public QObject {
    Q_OBJECT
private slots:

    /**
     * @brief removeFile
     * If the file test.conf exists, delete it.
     */
//    void removeFile();

    /**
     * @brief objectWithoutName
     * Try to persist an object without name.
     */
    void objectWithoutName();

    /**
     * @brief registerObjectOk
     */
    void registerObjectOk();

    /**
     * @brief notStoredParameter
     * Persist an object that has a property that would not be stored and verify
     * the behavior.
     * The Q_PROPERTY is not stored when the STORED is false.
     */
    void notStoredParameter();

    /**
     * @brief storedParameter
     * Try to persist an object with custom type and trivial properties and
     * verify if the object was successful persisted comparing the values when
     * recovering the object properties from settings system.
     */
    void storedParameter();

    void testQMLIntegration();
};

//void TestSerializer::removeFile() {
//    if ( QFile::exists("test.conf") ) {
//        QCOMPARE(QFile::remove("test.conf"), true);
//    }
//    QCOMPARE(QFile::exists("test.conf"), false);
//}

void TestSerializer::objectWithoutName() {
    Foo f;
    SettingsSerializer *serializer = SettingsSerializer::getInstance("test.conf");
    SettingsSerializer::Status status = serializer->registerObject(f);
    QCOMPARE(status, SettingsSerializer::QObjectWithoutName);
    serializer->deleteInstance("test.conf");
}

void TestSerializer::registerObjectOk() {
    Foo f;
    f.setObjectName("f");
    SettingsSerializer *serializer = SettingsSerializer::getInstance("test.conf");
    SettingsSerializer::Status status = serializer->registerObject(f);
    QCOMPARE(status, SettingsSerializer::Ok);
    serializer->deleteInstance("test.conf");
}

void TestSerializer::notStoredParameter() {
    Foo f;
    f.setObjectName("f");
    SettingsSerializer *serializer = SettingsSerializer::getInstance("test.conf");
    SettingsSerializer::Status status = serializer->registerObject(f);
    QCOMPARE(status, SettingsSerializer::Ok);
    f.setX(123);
    serializer->deleteInstance("test.conf");
    //
    serializer = SettingsSerializer::getInstance("test.conf");
    Foo f2;
    f2.setObjectName("f");
    status = serializer->registerObject(f2);
    QCOMPARE(status, SettingsSerializer::Ok);
    QCOMPARE(f2.x(), 0);
    serializer->deleteInstance("test.conf");
}

void TestSerializer::storedParameter() {
    SettingsSerializer *serializer = SettingsSerializer::getInstance("test.conf");
    Foo f;
    f.setObjectName("f");
    SettingsSerializer::Status status = serializer->registerObject(f);
    QCOMPARE(status, SettingsSerializer::Ok);
    f.setStatus(true);
    f.setCode(-5);
    f.setUcode(5);
    f.setCode16(-6);
    f.setUcode16(6);
    f.setCode32(-7);
    f.setUcode32(7);
    f.setCode64(-8);
    f.setUcode64(8);
    f.setPrice(18.5);
    f.setWeight(0.250);
    f.setX(44);
    f.setName("f1");
    f.setState(Foo::State::Complex);
    serializer->deleteInstance("test.conf");
    //
    serializer = SettingsSerializer::getInstance("test.conf");
    Foo f2;
    f2.setObjectName("f");
    status = serializer->registerObject(f2);
    QCOMPARE(status, SettingsSerializer::Ok);
    QCOMPARE(f2.status(), true);
    QCOMPARE(f2.code(), static_cast<qint8>(-5));
    QCOMPARE(f2.ucode(), static_cast<quint8>(5));
    QCOMPARE(f2.code16(), static_cast<qint16>(-6));
    QCOMPARE(f2.ucode16(), static_cast<quint16>(6));
    QCOMPARE(f2.code32(), static_cast<qint32>(-7));
    QCOMPARE(f2.ucode32(), static_cast<quint32>(7));
    QCOMPARE(f2.code64(), static_cast<qint64>(-8));
    QCOMPARE(f2.ucode64(), static_cast<quint64>(8));
    QCOMPARE(f2.price(), static_cast<float>(18.5));
    QCOMPARE(f2.weight(), static_cast<double>(0.250));
    QCOMPARE(f2.name(), QString("f1"));
    QCOMPARE(f2.state(), Foo::State::Complex);
    serializer->deleteInstance("test.conf");
}

void TestSerializer::testQMLIntegration()
{
    Bar::declareQmlSingletonType();

    QQuickView *view = new QQuickView;
    view->setSource(QUrl("qrc:/main.qml"));
    view->show();
    qApp->exec();
    delete view;

    view = new QQuickView;
    view->setSource(QUrl("qrc:/main.qml"));
    view->show();
    qApp->exec();
    delete view;
}

QTEST_MAIN(TestSerializer)
#include "testserializer.moc"

