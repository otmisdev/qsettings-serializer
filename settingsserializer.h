#ifndef SETTINGSSERIALIZER_H
#define SETTINGSSERIALIZER_H

#include <QDebug>
#ifdef QT_GUI_LIB
#include <QColor>
#endif
#include <QDir>
#include <QHash>
#include <QObject>
#include <QPair>
#include <QPointF>
#include <QRect>
#include <QRectF>
#include <QSettings>
#include <QSize>
#include <QMetaProperty>

#include <memory>

typedef QPair<QObject*, QMetaMethod> MetaSlot;

/**
 * @class SettingsSerializer
 * @brief The SettingsSerializer class can persist in QSettings system the
 * properties (Q_PROPERTY) of any object derived from QObject class.
 *
 * This class uses a singleton pattern.
 * This class can persist in QSettings system any property of an object derived
 * from QObject class.
 * The Q_PROPERTY has an attribute named STORED, the default value is true and
 * this attribute is used in order to differentiate what property will be
 * persisted or not in the settings file.
 * @code {.cpp}
 * // getting an instance of SettingsSerializer
 * SettinsSerializer *serializer = SettinsSerializer::getInstance();
 * // deleting an instance of SettingsSerializer
 *
 * @endcode
 * @warning When using SettingsSerializer with custom types in properties
 * remember that the notify method (signal) signature must use the complete
 * namespace of custom type, for example: let's suppose that the class name is
 * Bar and the custom type is nested declared in the Bar class definition and
 * the name is CustomEnum, so the signature of notify method must be
 * void onCustomPropertyChanged(Bar::CustomEnum) instead of void
 * onCustomPropertyChanged(CustomEnum).
 */
class SettingsSerializer : public QSettings {
    Q_OBJECT

public:
    enum Status {
        Ok = 0,
        QObjectWithoutName,
        ParameterNotSupported,
        WrongSignalSlotSignatures
    };

    static SettingsSerializer* getInstance(const QString& filename = QString("settings.conf"), const bool relativePath = true) {
        if ( m_ptrsHash->contains( filename ) ) {
            qDebug() << Q_FUNC_INFO << " Getting a existing one";
            return m_ptrsHash->value( filename );
        }

        qDebug() << Q_FUNC_INFO << " Generating a new Settings";
        m_ptrsHash->insert( filename, new SettingsSerializer( filename, relativePath ) );
        return m_ptrsHash->value( filename );
    }

    Status registerObject(QObject &obj);

    static void deleteAll() {
        foreach( QString key, m_ptrsHash->keys() ) {
            SettingsSerializer *ptr = m_ptrsHash->take( key );
            delete ptr;
        }
    }

    static void deleteInstance( const QString & filename ) {
        if ( m_ptrsHash->contains( filename ) ) {
            qDebug() << Q_FUNC_INFO << " Deleting an specific settings file";
            SettingsSerializer *ptr = m_ptrsHash->take( filename );
            delete ptr;
        }
    }

private:
    explicit SettingsSerializer(const QString& settingsFilename, const bool relativePath = true);
    virtual ~SettingsSerializer() {
#if ( QT_VERSION < QT_VERSION_CHECK(5, 0, 0) )
        delete m_ptrsHash;
#endif
        qDebug() << Q_FUNC_INFO;
    }

signals:

public slots:
    void genericUpdate(const QObject *senderObj, int signalIndex);

protected slots:
    void updateProperty();

protected:
    int recoverAndNotifyAll(QObject &obj);

private:

#if ( QT_VERSION < QT_VERSION_CHECK(5, 0, 0) )
    static QHash< QString, SettingsSerializer *> *m_ptrsHash;
#else
    static std::unique_ptr < QHash< QString, SettingsSerializer *> > m_ptrsHash;
#endif

};

#endif // SETTINGSSERIALIZER_H
