#include "settingsserializer.h"

#if ( QT_VERSION < QT_VERSION_CHECK(5, 0, 0) )
QHash < QString, SettingsSerializer* > *SettingsSerializer::m_ptrsHash = new  QHash < QString, SettingsSerializer* >();
#else
std::unique_ptr< QHash < QString, SettingsSerializer* > > SettingsSerializer::m_ptrsHash( new  QHash < QString, SettingsSerializer* >() );
#endif

SettingsSerializer::Status SettingsSerializer::registerObject(QObject &obj) {
    if ( ! obj.objectName().isEmpty() ) {
        QString groupName(obj.metaObject()->className());
        groupName.append("." + obj.objectName());
        beginGroup(groupName);
        int success = recoverAndNotifyAll(obj);
        endGroup();
        if ( success == 1 ) {
            return Ok;
        } else if ( success == -1 ) {
            return ParameterNotSupported;
        } else {
            return WrongSignalSlotSignatures;
        }
    } else {
        return QObjectWithoutName;
    }
}

SettingsSerializer::SettingsSerializer(const QString& settingsFilename, const bool relativePath)
    : QSettings( ( relativePath ? ( QDir::currentPath() + "/" + settingsFilename ) : settingsFilename), QSettings::NativeFormat)
{
}

void SettingsSerializer::genericUpdate(const QObject *obj, int signalIndex) {
    QMetaProperty prop;
    const QMetaObject* senderObj = obj->metaObject();
    QString groupName(senderObj->className());
    groupName.append("." + obj->objectName());
    beginGroup(groupName);
    int count = senderObj->propertyCount();
    int offset = senderObj->propertyOffset();
    // buscando propriedade que possui o mesmo signalIndex recebido a fim de
    // atualizar o arquivo de settings
    for (int i = offset; i < count; i++ ) {
        prop = senderObj->property(i);
        if ( prop.notifySignalIndex() == signalIndex ) {
            setValue(prop.name(), prop.read(obj));
            break;
        }
    }
    endGroup();
}

int SettingsSerializer::recoverAndNotifyAll(QObject &obj) {
    const QMetaObject *meta =  obj.metaObject();
    int count = meta->propertyCount();
    int offset = meta->propertyOffset();
    QMetaProperty prop;
    for ( int i = offset ; i < count; ++i ) {
        prop = meta->property(i);
        // recupera o valor do arquivo de settings caso a propriedade já esteja
        // gravada no arquivo
        const char* propName = prop.name();
        if ( contains(propName) ) {
            bool st;
            QVariant v = value(propName);
            int pType = prop.userType();
            switch ( pType ) {
            case QMetaType::Bool:
                st = obj.setProperty(propName, v.toBool());
                break;
            case QMetaType::Int:
                st = obj.setProperty(propName, v.toInt());
                break;
            case QMetaType::UInt:
                st = obj.setProperty(propName, v.toUInt());
                break;
            case QMetaType::LongLong:
                st = obj.setProperty(propName, v.toLongLong());
                break;
            case QMetaType::ULongLong:
                st = obj.setProperty(propName, v.toULongLong());
                break;
            case QMetaType::Float:
                st = obj.setProperty(propName, v.toFloat());
                break;
            case QMetaType::Double:
                st = obj.setProperty(propName, v.toDouble());
                break;
            case QMetaType::QChar:
                st = obj.setProperty(propName, v.toChar());
                break;
            case QMetaType::QString:
                st = obj.setProperty(propName, v.toString());
                break;
            case QMetaType::QByteArray:
                st = obj.setProperty(propName, v.toByteArray());
                break;
#ifdef QT_GUI_LIB
            case QMetaType::QColor:
                st = obj.setProperty(propName, v.value<QColor>());
                break;
#endif
            case QMetaType::QPoint:
                st = obj.setProperty(propName, v.value<QPoint>());
                break;
            case QMetaType::QPointF:
                st = obj.setProperty(propName, v.value<QPointF>());
                break;
            case QMetaType::QRect:
                st = obj.setProperty(propName, v.value<QRect>());
                break;
            case QMetaType::QRectF:
                st = obj.setProperty(propName, v.value<QRectF>());
                break;
            case QMetaType::QSize:
                st = obj.setProperty(propName, v.value<QSize>());
                break;
            case QMetaType::QVariantList:
                st = obj.setProperty(propName, v.value<QVariantList>());
                break;
            default:
                st = obj.setProperty(propName, v);
            }
            if ( ! st ) {
                qCritical() << "You may experience some problems...";
                qCritical() << "Q_PROPERTY" << propName << "TYPE" << prop.typeName() << "is not supported";
                return -1;
            }
        }
        // conecta o sinal que notifica a mudança no valor da propriedade ao
        // slot que atualiza o valor no arquivo de settings
        if ( prop.isStored() && prop.hasNotifySignal() ) {
            QMetaMethod notifySignal = prop.notifySignal();
            const QMetaObject *self = metaObject();
            MetaSlot pair(this, self->method(self->indexOfMethod("updateProperty()")));
#if ( QT_VERSION < QT_VERSION_CHECK(5, 0, 0) )
            bool conn = connect(&obj, notifySignal, pair.first, pair.second);
#else
            QMetaObject::Connection conn = connect(&obj, notifySignal, pair.first, pair.second);
#endif
            if ( ! conn ) {
                return -2;
            }
        }
    }
    return 1;
}

void SettingsSerializer::updateProperty() {
    genericUpdate(sender(), senderSignalIndex());
}
