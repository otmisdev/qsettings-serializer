# README #

The purpose of this repository is provide a easy way to use ***QSettings*** in order
to dynamically store properties of ***QObject*** derived classes. Using the class
***SettingsSerializer*** it's possible to "serialize" all the **Q_PROPERTY** of an
object when the **NOTIFY** signal is emitted.

Below the pseudo code describes a minimal class ***Foo*** with a property of type
***int*** and the following getter, setter and notify methods.

```
#!c++

class Foo : public QObject {
    Q_OBJECT
    Q_PROPERTY(int code READ code WRITE setCode NOTIFY onCodeChanged STORED true)
public:
    Foo(QObject *parent = 0)
    : QObject(parent)
    , m_code(-1)
    {
    }
    
    int code() const {
        return m_code;
    }

public slots:
    void setCode(int arg) {
        if ( m_code != arg ) {
            m_code = arg;
            emit onCodeChanged(arg);
        }
    }
    
signals:
    void onCodeChanged(int);

private:
    int m_code;
};

```

If we desire to store the attribute ***Foo::code*** into a settings file, and
persist this whenever there are changes in the property we can use the class
***SettingsSerializer*** as follows:

```
#!c++
// create a instance of Foo
Foo foo;
// give it a name in order to store/refer it in the settings file
foo.setObjectName("objFoo");
// get a singleton instance of SettingsSerializer
SettingsSerializer *serializer = SettingsSerializer::getInstance("preferences.conf");
// register the instance of the class Foo
SettingsSerializer::Status status = serializer->registerObject(foo);
if ( status != SettingsSerializer::Ok ) {
    qCritical("An error ocurred when register the object");
    qFatal("Quitting");
}
// after that every change in any property will be persisted at preferences.conf
foo.setCode(15);
// ...
// freeing the singleton instance
SettingsSerializer::deleteInstance();
```

# Examples #

Please, refer to the folder **examples**

# Tests #

Please, refer to the folder **tests**

# License and Copyrights #

This software uses LGPLv3 as the main license. Please find out a copy of LGPLv3
in the base directory of this repository (lgplv3.txt).

Copyright (C) 2015 Maquinas Agricolas Jacto S/A

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.